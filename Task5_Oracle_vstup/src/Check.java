public class Check {
 
    public static void main(String[] args) {
    	DynamicStringList list = new DynamicStringList();
        list.add("My");
        list.add("name");
        list.add("is");
        list.add("Yana");
        list.add("!");
        
        System.out.println(list.get());
        System.out.println(list.get(0));   
        System.out.println(list.toString());
        System.out.println(list.remove(3));
        System.out.println(list.toString());
        System.out.println(list.remove());
        System.out.println(list.toString());
        System.out.println(list.remove());
        System.out.println(list.toString());
        System.out.println(list.remove());
        System.out.println(list.toString());
        System.out.println(list.delete());
    }
}
